/**
 * Module dependencies.
 */

application = (function () {
	process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
	
	var express = require('express'),
		bodyParser = require("body-parser"),
		translationServer = require('./api/translator/translation_server'),
		route = require('./app/route');
    
    var redisClient = require("redis").createClient(),
    	app = global.app = module.exports = express(),
    	server = require('http').createServer(app);
    
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(express.static(__dirname + '/public'));
    server.listen(8888);
    
    translationServer.init(app);
}).call(this);
