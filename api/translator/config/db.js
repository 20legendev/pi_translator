var mongoose = require('mongoose'), config = require('./config');

mongoose.connect(config.mongo.db);

mongoose.connection.on('connected', function() {
	console.log('Mongoose connection open to ' + config.mongo.db);
});
mongoose.connection.on('error', function(err) {
	console.log('Mongoose connection error: ' + err);
});
mongoose.connection.on('disconnected', function() {
	console.log('Mongoose connection disconnected');
});

process.on('SIGINT', function() {
	mongoose.connection.close(function() {
		console.log('Mongoose default connection disconnected through app termination');
		process.exit(0);
	});
});
