var config = require('./config/config'),
	bingTranslator = require('bing-translate').init(config),
	db = require('./config/db');

var Word = require('./model/Word');

PiTranslator = {
		
		checkInput: function(data){
			var output = {};
			if(!data.word){
				return false;
			}
			output.word = data.word;
			output.from = data.from ? data.from : '',
			output.to = data.to ? data.to : 'en';
			return output;
		},
			
		translateWord: function(res, data){
			var wordLower = data.word.toLowerCase();
			Word.find({
				word: wordLower,
				from: data.from,
				to: data.to
			}, function(err, response){
				if(!err && response && response.length > 0){
					return res.json({
						status: 0,
						data: response[0].translated,
						msg: 'SUCCESS'
					});
				}else{
					bingTranslator.translate(data.word, data.from, data.to, function(err, response){
						if(err){
							return res.json({
								status: 1,
								msg: 'SERVICE_ERROR'
							});
						}
						var word = new Word({
							word: wordLower,
							from: data.from,
							to: data.to,
							translated: response,
							source: 1
						})
						word.save(function(error, data){
						});
						return res.json({
							status: 0,
							data: response,
							msg: 'SUCCESS'
						});
					});	
				}
			});
		}	
	}

TranslationServer = {
	init: function(app){
		this.app = app;
		this.handleRequest();
	},
	
	handleRequest: function(){
		this.app.get('/', function(req, res){
			PiTranslator.translateWord(res, {word: "Hello", from: 'en', to: 'ko'});
	    });
		
		this.app.post('/wsv/translation', function(req, res){
			if(!req.body){
				return res.json({
					status: 1,
					msg: 'ERROR'
				});
			}
			var data = PiTranslator.checkInput(req.body);
			if(!data){
				return res.json({
					status: 1,
					msg: 'ERROR'
				});
			}
			PiTranslator.translateWord(res, data);	
		});
	}
}

module.exports = TranslationServer;