var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var wordSchema = new Schema({
	  word: String,
	  from:   String,
	  to: String,
	  translated: String,
	  source: Number
});
var Word = mongoose.model('Word', wordSchema);

module.exports = Word;